import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
import Products from './components/Products/Products';
import Header from './components/Header/Header';

import './App.css';


class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      products: [],
      isLoading: true,
      selector: null,
      title: '',
      description: '',
      price: ''
    }
  }

  componentDidMount = () => {
    this.getProductsData()
  }

  getProductsData = () => {
    fetch("https://fakestoreapi.com/products")
      .then((data) => data.json())
      .then((data) => {
        this.setState({
          products: data,
          isLoading: false
        })
      })
  }

  getSelector = (id) => {
    const data = this.state.products.filter((eachProduct) => eachProduct.id === Number(id))
    const result = data.pop()
    this.setState({
      selector: result,
      title: result.title,
      description: result.description,
      price: result.price
    })
  }

  changeTitle = (event) => {

    const id = this.state.selector.id

    const data = this.state.products.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.title = event.target.value
        return eachProduct
      }
      return eachProduct
    })
    this.setState({
      products: data,
      title: event.target.value
    })

  }

  changeDescription = (event) => {
    const id = this.state.selector.id

    const data = this.state.products.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.description = event.target.value
        return eachProduct
      }
      return eachProduct
    })
    this.setState({
      products: data,
      description: event.target.value
    })
  }

  changePrice = (event) => {
    const id = this.state.selector.id

    const data = this.state.products.map((eachProduct) => {
      if (eachProduct.id === id) {
        eachProduct.price = event.target.value
        return eachProduct
      }
      return eachProduct
    })

    this.setState({
      products: data,
      price: event.target.value
    })
  }

  saveUpdate = () => {
    this.setState({
      selector: null,
      title: "",
      description: '',
      price: ''
    })
  }

  render() {
    return this.state.isLoading ?
      
        <div className='vh-100 d-flex justify-content-center align-items-center bg-gray'> <Loader /> </div> :
        <>
        <Header />
        <Products
          details={this.state}
          saveUpdate={this.saveUpdate}
          changePrice={this.changePrice}
          changeDescription={this.changeDescription}
          changeTitle={this.changeTitle}
          getSelector={this.getSelector}
        />
      </>
  }
}

export default App;
