import React from 'react'

function Header(props) {
    const isLogged = props.isLogged
    const updateToggleLogger = props.updateToggleLogger
    const toggleSign =  isLogged ? 'Logout' : 'Signup'

    const logout=()=>{
        updateToggleLogger()
    }


    return (
        <div>
            <nav className="navbar navbar-expand-md navbar-light bg-warning ">
                <div className="container-fluid">
                    <span className='fw-bold'>SHOP ON </span>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav flex-row justify-content-center">
                            <li className="nav-item me-3">
                                <span > Home </span>
                            </li>
                            <li className="nav-item me-3">
                                <span >Products</span>
                            </li>
                            <li className="nav-item me-3">
                                <span onClick={logout}>{toggleSign}</span>
                            </li>
                            <i className="fa-solid fa-cart-plus"></i>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header