import React from 'react'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'


import '../../App.css'

function Item(props) {

    const { id, image, title, description, price, rating } = props.product

    function selectorId(event) {
        props.getSelector(event.target.id)
    }

    return (
        <div className='col-12 col-lg-5 bg-white mb-3 m-lg-3 text-center p-2 d-flex flex-column justify-content-between align-items-center'>

            <img src={image} alt='product' className='w-50' />

            <h4 className='fw-bold'>{title}</h4>
            <span className='mini-text'>{description}</span> <br />
            <h4 className='text-danger fw-bold'>Price: ${price}</h4>
            <div>
                <span>Rating:{rating.rate}</span> | <span>by: {rating.count} users</span> <br />
            </div>

            <span><button className='me-1'>-</button>Qnt:<button className='ms-1'>+</button></span> <br />

            <div>
                <button className='btn-primary' id={id} onClick={selectorId}>Select</button>
            </div>
        </div>
    )
}


export default Item