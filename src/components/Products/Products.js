import React, { Component } from 'react'
import Item from '../Item/Item' 

import '../../App.css'

class Products extends Component {


  getSelector =(id)=>{
    this.props.getSelector(id)
  }

  changeDescription =(event)=>{
    this.props.changeDescription(event)
  }

  changeTitle =(event)=>{
    this.props.changeTitle(event)
  }

  changePrice =(event)=>{
    this.props.changePrice(event)
  }

  SaveUpdate =(event)=>{
    this.props.saveUpdate(event)
  }

  render() {

 
    const {products,description,price,selector,title} = this.props.details

    return (
      <div className='container-fluid'>
      <div className='row flex-column-reverse flex-lg-row'>

        <div className='col-sm-12 col-lg-6  bg-primary container-fluid d-flex flex-column  cardFlow'>
          <div className='row height'>
            {products.map((product) => {
              return <Item key={product.id} product={product} getSelector={this.getSelector} />
            })}
          </div>
        </div>

        <div className='col-sm-12 col-lg-6 d-flex flex-column justify-content-center align-items-center'>
          {(!selector) ?
            <h1>Select the product to update</h1> :
            <>
              <div className='d-flex flex-column p-5 border border-success'>
                <h4>Update Details</h4>
                <label htmlFor='title' className='fw-bold'>Title </label>
                <input className='rounded' type='text' id='title' value={title} onChange={this.changeTitle} />
                <label htmlFor='description' className='fw-bold'>Description </label>
                <textarea className='rounded' rows="5" value={description} id='description' onChange={this.changeDescription} />
                <label htmlFor='price' className='fw-bold'> Price </label>
                <input className='rounded' type='number' min="1" step="1" id='price' value={price} onChange={this.changePrice} />
                <div className='mt-3'>
                  <button onClick={this.SaveUpdate} className='btn-primary'>Save</button>
                </div>
              </div>
            </>
          }

        </div>

      </div>

    </div>
    )
  }
}

export default Products
